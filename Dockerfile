FROM node:lts

ENV CARGO_HOME="/root/.cargo"
ENV RUSTUP_HOME="/root/.rustup"
ENV PATH="/root/.cargo/bin:${PATH}"
ENV RUST_BACKTRACE=1

RUN apt-get update && apt-get install -y cmake python3 python3-pip && \
    LATEST_VAULT=$(curl -s https://latest.cmm.io/vault) && \
    if [ "$(uname -m)" = "aarch64" ]; then export ARCH="arm64"; else export ARCH="amd64"; fi && \
    wget https://releases.hashicorp.com/vault/${LATEST_VAULT}/vault_${LATEST_VAULT}_linux_${ARCH}.zip && \
    unzip vault_${LATEST_VAULT}_linux_${ARCH}.zip && \
    rm vault_${LATEST_VAULT}_linux_${ARCH}.zip && \
    mv vault /usr/bin/vault && \
    pip3 install --break-system-packages j2cli && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && \
    rustup target add wasm32-unknown-unknown && \
    npm install -g wrangler && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/bin/bash"]
