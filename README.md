# Rust WASM

Docker image for use in CI pipelines building rust projects to WASM (usually for Cloudflare Workers).

Based on the `node:lts` image with rust and wrangler preinstalled.

`registry.gitlab.com/cmmarslender/rust-wasm:main`
